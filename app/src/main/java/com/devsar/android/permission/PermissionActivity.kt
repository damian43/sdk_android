package com.devsar.android.permission

import android.app.Activity
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import androidx.annotation.RequiresApi

class PermissionActivity : Activity() {

    private val permissionRequest = 100

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (PermissionData.isSinlgePermission()) {
            requestPermissions(arrayOf(PermissionData.getPermission()), permissionRequest)
        } else {
            requestPermissions(
                PermissionData.getPermissions().toArray(
                    arrayOfNulls<String>(PermissionData.getPermissions().size)
                ),
                permissionRequest
            )
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (PermissionData.isSinlgePermission()) {
            if (requestCode == permissionRequest) {
                PermissionData.setResult(grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
            } else {
                PermissionData.setResult(false)
            }
        } else {
            val permissionsResults = HashMap<String, Boolean>()
            for (i in grantResults.indices) {
                permissionsResults[permissions[i]] = (grantResults[i] == PackageManager.PERMISSION_GRANTED)
            }
            PermissionData.setResults(permissionsResults)
        }
        finish()
    }
}