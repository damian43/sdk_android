package com.devsar.android.permission

import java.util.*

interface PermissionsListener {
    fun onPermissionsListener(results: HashMap<String, Boolean>)
}