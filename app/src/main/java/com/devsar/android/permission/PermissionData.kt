package com.devsar.android.permission

object PermissionData {

    var p : String = ""
    var ps = ArrayList<String>()
    lateinit var l: PermissionListener
    lateinit var pl: PermissionsListener

    fun init(permission: String, listener: PermissionListener) {
        p = permission
        l = listener
    }

    fun init(permissions: ArrayList<String>, listener: PermissionsListener) {
        ps = permissions
        pl = listener
    }

    fun getPermission() : String = p

    fun getPermissions() : ArrayList<String> = ps

    fun setResult(result: Boolean) {
        l.onPermissionListener(result)
    }

    fun setResults(results: HashMap<String, Boolean>) {
        pl.onPermissionsListener(results)
    }

    fun isSinlgePermission() : Boolean = p.isNotEmpty()
}