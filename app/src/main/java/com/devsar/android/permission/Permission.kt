package com.devsar.android.permission

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

class Permission {

    companion object {
        const val GRANTED = 0
        const val DENIED = -1
        const val NEVER_ASK_AGAIN = 1
    }

    /**
     * Return stare of permission
     * @param permission Permission
     * @return state of permission
    **/
    fun getState(activity: Activity, ctx: Context, permission: String) : Int {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(ctx, permission) == PackageManager.PERMISSION_GRANTED) {
                GRANTED
            } else {
                if (!ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
                    NEVER_ASK_AGAIN
                } else {
                    DENIED
                }
            }
        } else {
            GRANTED
        }
    }

    /**
     * Return state of permissions
     * @param permissions Permissions list
     * @return Map with states of permissions
     **/
    fun getStates(activity: Activity, ctx: Context, permissions: List<String>) : HashMap<String, Int> {
        val result = HashMap<String, Int>()
        for (p in permissions) {
            result[p] = getState(activity, ctx, p)
        }
        return result
    }

    /**
     * Check if permission is granted
     * @param permission Permission
     * @return boolean isGranted
     **/
    fun isGranted(ctx: Context, permission: String) : Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ContextCompat.checkSelfPermission(ctx, permission) == PackageManager.PERMISSION_GRANTED
        } else {
            true
        }
    }

    /**
     * Check if permissions are granted
     * @param permissions Permissions list
     * @return boolean areGranted
     **/
    fun areGranted(ctx: Context, permissions: List<String>) : Boolean {
        for (p in permissions) {
            if (!isGranted(ctx, p)) {
                return false
            }
        }
        return true
    }

    /**
     * Returns if justification for request permission must be shown
     * @param permission Permission
     * @return boolean if should show request permission rationale
     **/
    fun shouldShowRequestPermissionRationale(activity: Activity, permission: String) : Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)
        } else {
            false
        }
    }

    /**
     * Request a permission
     * @param permission Permissions list
     * @param action action to invoke when permission has been requested (boolean is granted)
     **/
    fun requestPermission(ctx: Context, permission: String, action: (v: Boolean) -> Unit) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (isGranted(ctx, permission)) {
                action.invoke(true)
            } else {
                PermissionData.init(permission, object : PermissionListener {
                    override fun onPermissionListener(granted: Boolean) {
                        action.invoke(granted)
                    }
                })
                ctx.startActivity(Intent(ctx, PermissionActivity::class.java))
            }
        } else {
            action.invoke(true)
        }
    }

    /**
     * Request multiple permissions
     * @param permissions Permissions list
     * @param action action to invoke when permissions has been requested (Map <Permission,Granted>)
     **/
    fun requestPermissions(ctx: Context, permissions: List<String>, action: (r: HashMap<String, Boolean>) -> Unit) {
        val permissionsToRequest = ArrayList<String>()
        val result = HashMap<String, Boolean>()
        for (p in permissions) {
            result[p] = isGranted(ctx, p)
            if (!result[p]!!) {
                permissionsToRequest.add(p)
            }
        }
        if (permissionsToRequest.isNotEmpty()) {
            PermissionData.init(permissionsToRequest, object : PermissionsListener {
                override fun onPermissionsListener(results: java.util.HashMap<String, Boolean>) {
                    run {
                        for (r in results) {
                            result[r.key] = r.value
                        }
                        action.invoke(result)
                    }
                }
            })
            ctx.startActivity(Intent(ctx, PermissionActivity::class.java))
        } else {
            action.invoke(result)
        }
    }
}