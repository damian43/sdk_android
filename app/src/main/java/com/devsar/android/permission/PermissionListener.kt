package com.devsar.android.permission

interface PermissionListener {
    fun onPermissionListener(granted: Boolean)
}