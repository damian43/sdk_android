package com.devsar.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.devsar.android.R;

public class TitleTextView extends LinearLayout {

    private TextView textView;

    public TitleTextView(Context context) {
        super(context);
        init(context, null);
    }

    public TitleTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context ctx, @Nullable AttributeSet attrs) {
        LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (inflater != null) inflater.inflate(R.layout.title_text_view, this, true);
        textView = findViewById(R.id.textView);
    }

    public void setText(String text) {
        textView.setText(text);
    }
}